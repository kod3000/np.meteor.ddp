﻿using Newtonsoft.Json;
using Newtonsoft.Json.Linq;
using System;
using System.Collections.Generic;
using System.IO;
using System.Net.WebSockets;
using System.Text;
using System.Threading;
using System.Threading.Tasks;

namespace NP.Meteor.DDP
{
    public class DdpClient : IDisposable
    {
        public event EventHandler<String> Connected;
        public event EventHandler<String> Disconnected;
        public event EventHandler<String> Subscribed;
        public event EventHandler<Message> Unsubscribed;

        public event EventHandler<Message> Added;
        public event EventHandler<Message> Changed;
        public event EventHandler<Message> Removed;
        public event EventHandler<MethodResult> MethodResult;

        public event EventHandler<MeteorError> MeteorError;
        public event EventHandler<ClientError> ClientError;


        private ClientWebSocket _socket;
        private Uri _uri;
        private String _sessionId;

        //Contains a dictionary, the object itself is a dicitonary of items.
        private Dictionary<string, Dictionary<string, JObject>> collections = new Dictionary<string, Dictionary<string, JObject>>();

        public DdpClient(String url, bool use_https = true)
        {
            this._uri = new Uri(String.Format(((use_https) ? "wss" : "ws") + "://{0}/websocket", url));
            System.Net.ServicePointManager.MaxServicePointIdleTime = int.MaxValue;  //Ensure we keep the socket open 
        }

        /// <summary>
        /// Connect to Meteor
        /// </summary>
        /// <param name="max_attempts">Optional, 0 or missing, try to connect forever. If > 0 then it will only try to connect that many times before throwing ClientError</param>
        /// <param name="aToken">Cancellation Token, allows you to cancel the connection externally.</param>
        public void Connect(int max_attempts = 0, CancellationToken aToken = default(CancellationToken))
        {
            this.ConnectAsync(max_attempts, aToken).Wait();
        }

        public void Method(String callId, String method, params dynamic[] args)
        {
            this.MethodAsync(callId, method, args).Wait();
        }

        public void Subscribe(String callId, String subscription, params dynamic[] args)
        {
            this.SubscribeAsync(callId, subscription, args).Wait();
        }

        public void Unsubscribe(String callId, String method)
        {
            this.UnSubscribeAsync(callId, method).Wait();
        }

        int maxAttempts = 0;
        CancellationToken theToken;
        public async Task ConnectAsync(int max_attempts = 0, CancellationToken aToken = default(CancellationToken))
        {
            //Save these so they can be used in the disconnect handler
            maxAttempts = max_attempts;
            theToken = aToken;

            int attempt = 1;
            do
            {

                if (this._socket != null)
                    this._socket.Dispose();

                this._socket = new ClientWebSocket();
                //                this._socket.Options.KeepAliveInterval = ;

                await this._socket.ConnectAsync(this._uri, aToken)
                    .ContinueWith(s =>
                    {
                        return this.Send(new
                        {
                            msg = "connect",
                            version = "1",
                            support = new string[] { "1", "pre2", "pre1" }
                        });
                    })
                    .ContinueWith(s =>
                    {
                        return SubscriberLoop();
                    });

                if (this._socket.State != WebSocketState.Open)
                {
                    Console.WriteLine("Could not connect to server, " + ((max_attempts > 0) ? "retrying: " + attempt + " of " + max_attempts : "retrying..."));
                    attempt++;
                    Thread.Sleep(2000);
                }


            } while (this._socket.State != WebSocketState.Open && (max_attempts == 0 || attempt <= max_attempts));
        }

        /// <summary>
        /// Calls a meteor method
        /// </summary>
        /// <param name="callId">uuid of method call</param>
        /// <param name="method"> method name</param>
        /// <param name="args"> array of arguments</param>
        /// <returns></returns>
        public async Task MethodAsync(String callId, String method, params dynamic[] args)
        {
            await this.Send(new
            {
                msg = "method",
                method = method,
                @params = args,
                id = callId
            });
        }

        /// <summary>
        /// Subscribes to a method
        /// </summary>
        /// <param name="subId">GUUID</param>
        /// <param name="subName">Name of subscription</param>
        /// <param name="args">Array of arguments to pass to the subscription</param>
        /// <returns></returns>
        public async Task SubscribeAsync(String subId, String subName, params dynamic[] args)
        {
            Console.WriteLine("Subscribing to: " + subId);

            collections.Remove(subName);

            await this.Send(new
            {
                msg = "sub",
                name = subName,
                @params = args,
                id = subId
            }
            );
        }

        /// <summary>
        /// Unsubscribes from a subscription
        /// </summary>
        /// <param name="subId">The id of the subscription to unsubscribe</param>
        /// <param name="subName">The name of the subscription</param>
        /// <returns></returns>
        public async Task UnSubscribeAsync(String subId, String subName)
        {
            collections.Remove(subName);

            await this.Send(new
            {
                msg = "unsub",
                name = subName,
                id = subId
            }
            );
        }

        object aLock = new object();
        private async Task Send(dynamic message)
        {
            ArraySegment<byte> segment = new ArraySegment<byte>(Encoding.UTF8.GetBytes(JsonConvert.SerializeObject(message)));

            try
            {
                await Task.Run(() =>
                {
                    return this._socket.SendAsync(segment, WebSocketMessageType.Text, true, CancellationToken.None);
                });
            }
            catch (Exception e)
            {
                ;//Trying to send on a disposed socket
            }
        }

        private async Task SubscriberLoop()
        {
            Int32 bufferSize = 1024 * 32;
            byte[] buffer = new byte[bufferSize];
            MemoryStream stream = new MemoryStream(bufferSize);
            String msg_json = "";
            ArraySegment<byte> segment = new ArraySegment<byte>(buffer);

            try
            {
                while (this._socket.State == WebSocketState.Open)
                {
                    Thread.Sleep(5);   //Prevent blocking the entire thread
                    WebSocketReceiveResult result = null;
                    try
                    {
                        result = await this._socket.ReceiveAsync(segment, CancellationToken.None);
                    }
                    catch (Exception e)
                    {
                        continue;//Trying to receive on a disposed socket
                    }
                    if (result == null)
                        continue;

                    if (result.MessageType == WebSocketMessageType.Close)
                    {

                    }
                    else if (!result.EndOfMessage)
                    {
                        stream.Write(buffer, 0, result.Count);
                    }
                    else
                    {
                        stream.Write(buffer, 0, result.Count);
                        msg_json = Encoding.UTF8.GetString(stream.ToArray());
                        dynamic message = JsonConvert.DeserializeObject<dynamic>(msg_json);

                        if (this._sessionId == null && message.session != null)
                        {
                            this._sessionId = message.session.ToString();
                            this.Connected(this, "Connected to Meteor, SessionId: " + this._sessionId);
                        }
                        else if (message == null || message.msg == null)
                        {
                            if (message.server_id == null)
                                throw new DdpClientException(String.Format("Unknown message: {0}", message));
                        }
                        else
                        {
                            String msgType = message.msg.ToString();
                            string id = "";
                            if (message.id != null)
                                id = message.id.ToString();


                            string collectionName = "";
                            if (message.collection != null)
                                collectionName = message.collection.ToString();

                            JObject row = null;
                            if (message.fields != null)
                                row = JObject.Parse(message.fields.ToString());

                            // pull the collection if we have it
                            Dictionary<string, JObject> collection = null;
                            if (!string.IsNullOrEmpty(collectionName))
                                collections.TryGetValue(collectionName, out collection);

                            switch (msgType)
                            {
                                case "ping":
                                    await this.Send(new { msg = "pong" });
                                    break;
                                case "error":
                                    if (this.MeteorError != null)
                                        this.MeteorError(this, new MeteorError(message.reason.ToString(), message.offendingMessage));
                                    break;

                                case "added":

                                    if (string.IsNullOrEmpty(collectionName))
                                        break;

                                    if (collection == null)
                                        collections[collectionName] = collection = new Dictionary<string, JObject>();

                                    collection.Add(id, row);

                                    if (this.Added != null)
                                        this.Added(this, new Message(message.collection.ToString(), collection, row));

                                    break;

                                case "removed":

                                    if (string.IsNullOrEmpty(collectionName))
                                        break;

                                    if (collection == null)
                                        break;

                                    JObject removedRow;
                                    if (collection.TryGetValue(id, out removedRow))
                                    {
                                        if (this.Removed != null)
                                            this.Removed(this, new Message(collectionName, collection, removedRow));
                                    }

                                    break;
                                case "changed":

                                    if (string.IsNullOrEmpty(collectionName))
                                        break;

                                    //Delete calls change too
                                    if (row == null)
                                        break;

                                    // create a collection if we don't have one
                                    if (collection == null)
                                        collections[collectionName] = collection = new Dictionary<string, JObject>();

                                    // create a row if we don't have one
                                    JObject oldRow = null;
                                    if (!collection.TryGetValue(id, out oldRow))
                                        collection[id] = oldRow = new JObject();

                                    //Update the oldRow with the new row's values.
                                    foreach (var kvp in row)
                                        oldRow[kvp.Key] = kvp.Value;

                                    if (this.Changed != null)
                                        this.Changed(this, new Message(message.collection.ToString(), collection, oldRow));

                                    break;

                                case "nosub":   //Called when unsubscribe is called

                                    if (string.IsNullOrEmpty(collectionName))
                                        break;

                                    collections.Remove(collectionName);

                                    if (this.Unsubscribed != null)
                                        this.Unsubscribed(this, new Message(id, null));

                                    break;

                                //A Subscription is ready
                                case "ready":

                                    if (this.Subscribed != null)
                                        this.Subscribed(this, message.subs[0].ToString());

                                    break;
                                //Result of a Meteor.call()
                                case "result":
                                    if (this.MethodResult != null)
                                    {
                                        Meteor.DDP.MethodResult.MethodError error = null;
                                        if (message.error != null)
                                        {
                                            error = new MethodResult.MethodError(
                                                message.error.error.ToString(),
                                                message.error.reason.ToString(),
                                                message.error.message.ToString(),
                                                message.error.errorType.ToString());
                                        }
                                        this.MethodResult(this, new MethodResult(message.id.ToString(), error, message.result?.ToString()));
                                    }
                                    break;

                                //Dont worry about these events, occurs when a Meteor.Method() updates a record.
                                case "updated":
                                    //Console.WriteLine(message.ToString());
                                    break;

                                default:
                                    Console.WriteLine("Unknown Meteor Return type", message.ToString());

                                    break;
                            }
                        }
                        stream.Dispose();
                        stream = new MemoryStream();
                    }

                }
            }
            catch (Exception x)
            {
                collections.Clear();

                //Disconnected
                if (this._socket.State != WebSocketState.Open)
                {
                    if (this.Disconnected != null)
                        this.Disconnected(this, "Meteor is disconnected, state: " + this._socket.State.ToString());

                    await ConnectAsync(maxAttempts, theToken);
                }
                else //Fatal error
                {
                    if (this.ClientError != null)
                        this.ClientError(this, new ClientError(x));
                }
            }
            finally
            {
                this._sessionId = null;
                stream.Dispose();
            }
        }

        public void Dispose()
        {
            if (this._socket != null)
            {
                this._socket.Dispose();
            }
        }

        public WebSocketState WebSocketState
        {
            get { return this._socket.State; }
        }

        public string Uri
        {
            get { return this._uri.ToString(); }
        }
    }
}