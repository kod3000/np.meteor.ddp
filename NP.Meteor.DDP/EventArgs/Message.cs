﻿using Newtonsoft.Json.Linq;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace NP.Meteor.DDP
{
    public class Message
    {
        public String Id { get; private set; }
        public Dictionary<string, JObject> Collection { get; private set; }
        public JObject AffectedRow { get; private set; }

        internal Message(String id, Dictionary<string,JObject> collection, JObject affectedRow = null)
        {
            this.Id = id;
            this.Collection = collection;
            this.AffectedRow = affectedRow;
        }
    }
}
