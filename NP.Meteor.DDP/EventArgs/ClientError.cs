﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace NP.Meteor.DDP
{
    public class ClientError
    {
        public Exception Exception { get; private set; }
        internal ClientError(Exception x)
        {
            this.Exception = x;
        }

    }
}
