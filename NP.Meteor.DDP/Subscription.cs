﻿using System.Collections.Generic;

namespace NP.Meteor.DDP
{
    public class Subscription
    {
        public string name;
        public Dictionary<string, object> args = new Dictionary<string, object>();

        public Subscription(string theName, Dictionary<string, object> theArgs)
        {
            this.name = theName;
            this.args = theArgs;
        }
    }
}
