﻿
using NP.Meteor.DDP;
using System;
using System.Collections.Generic;
using System.Net.WebSockets;
using System.Threading;

namespace NP.Meteor.DDP_Demo
{
    class Program
    {
        static Dictionary<string, Subscription> listOfSubscriptions = new Dictionary<string, Subscription>();
        static DdpClient client;
        static int max_conn_attempts = 1;
        static bool reconnect_if_disconnected = true;

        static void Main(string[] args)
        {
            //Create a list of Subscriptions, this allows you to automatically subscribe,
            //(and re-subscribe) to a whole list of subscriptions if you are disconnected.
            /*
            listOfSubscriptions.Add(Guid.NewGuid().ToString("N"),
                                new Subscription("games", new Dictionary<string, object>
                                {
                                    { "api_key" , "INSECURE" }
                                }));
                                */

            listOfSubscriptions.Add(Guid.NewGuid().ToString("N"),
                                new Subscription("game", new Dictionary<string, object>
                                {
                                    { "api_key" , "INSECURE" },
                                    { "game_uuid","d570b0a4-8710-11e9-bf43-029247acfb0c" }
                                }));

            listOfSubscriptions.Add(Guid.NewGuid().ToString("N"),
                    new Subscription("objects", new Dictionary<string, object>
                    {
                                    { "api_key" , "INSECURE" },
                                    { "game_uuid","d570b0a4-8710-11e9-bf43-029247acfb0c" }
                    }));

            using (client = new DdpClient("localhost:3000",false))
            {
                //Initialise Callbacks
                client.Connected += OnConnected;
                client.Disconnected += OnDisconnected;
                client.Subscribed += onSubscribed;
                client.Unsubscribed += onUnSubscribed;

                client.Added += OnAdded;
                client.Changed += onChanged;
                client.Removed += onRemoved;
                client.MethodResult += OnMessageResult;

                client.MeteorError += OnMeteorError;
                client.ClientError += OnClientError;

                Console.WriteLine("Conencting to Meteor " + client.Uri);

                client.Connect(max_conn_attempts);  //If max_conn_attempts = 0 will block until connected.
                if (client.WebSocketState != WebSocketState.Open)
                {
                    Console.WriteLine("Could not connect to server..");
                    Console.ReadLine();
                    Environment.Exit(0);
                }

                MakeSubscriptions();
                //DemoMethodCall();

                Thread.Sleep(2000);
                Console.WriteLine("Press any key to unsubscribe..");
                Console.ReadLine();
                CancelSubscriptions();

                Thread.Sleep(2000);
                Console.WriteLine("Press any key to re-subscribe..");
                Console.ReadLine();
                MakeSubscriptions();

                Console.WriteLine("Press any key to quit..");
                Console.ReadLine();
            }
        }


        static void DemoMethodCall()
        {
            string call_id = Guid.NewGuid().ToString("N");
            client.Method(call_id, "createGame", new
            {
                api_key = "INSECURE",
                name = "New Game 1"
            });
        }

        /// <summary>
        /// Will connect to all your subscriptions
        /// </summary>
        static void MakeSubscriptions()
        {
            foreach (KeyValuePair<string,Subscription> aKvp in listOfSubscriptions)
                client.Subscribe(aKvp.Key, aKvp.Value.name, aKvp.Value.args);
        }
                    
        /// <summary>
        /// Will unsubscribe from your list of subscriptions
        /// </summary>
        static void CancelSubscriptions()
        {
            foreach (KeyValuePair<string, Subscription> aKvp in listOfSubscriptions)
                client.Unsubscribe(aKvp.Key, aKvp.Value.name);
        }


        #region Callbacks
        /// <summary>
        /// Meteor returned an error, probably because you have passed it invalid data.
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        static void OnMeteorError(object sender, MeteorError e)
        {
            Console.Error.WriteLine("Error: " + e.Reason.ToString());
            Console.Error.WriteLine("Original Message: " + e.OriginalMessage.ToString());
        }

        /// <summary>
        /// The Meteor Client object has thrown an error, most likely your meteor server is unreachable,
        /// or you have been disconnected.
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        static void OnClientError(object sender, ClientError e)
        {
            Console.WriteLine("Fatal Error: {0}", e.Exception.Message);
            Console.WriteLine("Stack Trace: {0}", e.Exception.StackTrace);
            DdpClient client = (DdpClient)sender;

            ///Disconnected, Handle reconnects here.
            if (client.WebSocketState != WebSocketState.Open)
            {
                Console.WriteLine("Disconnected from Meteor.");
                if (reconnect_if_disconnected)
                { 
                    Console.WriteLine("You have enabled reconnect_if_disconencted");
                    if (client.WebSocketState != WebSocketState.Open)
                    {
                        client.Connect();
                        if (client.WebSocketState == WebSocketState.Open)
                            MakeSubscriptions();
                    }
                }
            }
        }

        static void OnAdded(object sender, Message msg)
        {
            Console.WriteLine("Collection: "+msg.Id + ", row ADDED: "+msg.AffectedRow.ToString());
        }

        static void onChanged(object sender, Message msg)
        {
            Console.WriteLine("Collection: " + msg.Id + ", row CHANGED: " + msg.AffectedRow.ToString());
        }

        static void onRemoved(object sender, Message msg)
        {
            Console.WriteLine("Collection: "+msg.Id + ", row REMOVED: "+ msg.AffectedRow.ToString());
        }

        static void onSubscribed(object sender, string subscription_id)
        {
            Console.WriteLine("Subscription: "+ subscription_id+", READY");
        }

        static void onUnSubscribed(object sender, Message msg)
        {
            Console.WriteLine("Subscription: "+msg.Id + " has been removed.");
        }

        static void OnMessageResult(object sender, MethodResult e)
        {
            Console.WriteLine("Method call result:{0}:  {1}", e.CallId, e.Result);
            if (e.Error != null)
                Console.WriteLine("Error: {0}", e.Error);
        }

        static void OnConnected(object sender, String msg)
        {
            Console.WriteLine(msg);
        }

        static void OnDisconnected(object sender, String msg)
        {
            Console.WriteLine(msg);
        }

        #endregion
    }
}
