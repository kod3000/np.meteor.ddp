Inline-style: 
![Missing image](https://bitbucket.org/n8ores/np.meteor.ddp/raw/ce3d4831739d61d81cbb70af910c0383f728ba81/demo.png "Demo")

# NP.Meteor.DDP

This is a simple and robust implementation of Meteor DDP integration with .NET/C# based upon [sgaluza/Meteor.DDP](https://github.com/sgaluza/Meteor.DDP).
sgaluza/Meteor.DDP has been extended to implement a number of additional features:

1. Supports an internal collection (JArray) for each subscription. Each method callback returns the affected row as a JObject and the entire collection as a JArray.
2. Optional Automatic re-connect (incase your connection goes down).
3. Subscribe to a list of publications.
4. Support for un-subscribing.
5. Improved naming convetions (Publications and Method Calls).
6. Handle a wider variety of server responses.
7. Data is returned via EventCallbacks as JSON.
8. Supports HTTPS (WSS - Default) and HTTP (WS)


Runtime needs a platform that provides `System.Net.WebSockets`, in practice, Windows 8 and above.  
The only external dependencies are [Newtonsoft.Json](https://www.nuget.org/packages/Newtonsoft.Json/) and [Nito.AsyncEx](https://www.nuget.org/packages/Nito.AsyncEx/).


## Connection
```
using (client = new DdpClient("localhost:3000"))
{
    //Initialise Callbacks
    client.Connected += OnConnected;
    client.Subscribed += onSubscribed;
    client.Unsubscribed += onUnSubscribed;

    client.Added += OnAdded;
    client.Changed += onChanged;
    client.Removed += onRemoved;
    client.MethodResult += OnMessageResult;

    client.MeteorError += OnMeteorError;
    client.ClientError += OnClientError;

	client.Connect(max_conn_attempts);  //If max_conn_attempts = 0 will block until connected.
}
```

##  Subscriptions

### Meteor Side

```
Meteor.publish('games', function(){
    return SomeCollection.find({});
});
```

### .NET Side

```
listOfSubscriptions.Add(Guid.NewGuid().ToString("N"),
                    new Subscription("games", new Dictionary<string, object>
                    {
                        { "api_key" , "INSECURE" }
                    }));

static void MakeSubscriptions()
{
    foreach (KeyValuePair<string,Subscription> aKvp in listOfSubscriptions)
        client.Subscribe(aKvp.Key, aKvp.Value.name, aKvp.Value.args);
}

static void CancelSubscriptions()
{
    foreach (KeyValuePair<string, Subscription> aKvp in listOfSubscriptions)
        client.Subscribe(aKvp.Key, aKvp.Value.name, aKvp.Value.args);
}
```

## Meteor Methods

### Meteor Side

```
Meteor.methods({
    createGame: function (p){
      console.log("I was called from .NET", p);
    }
  });
```

### .NET Side

```
static void DemoMethodCall()
{
    string call_id = Guid.NewGuid().ToString("N");
    client.Method(call_id, "createGame", new
    {
        api_key = "INSECURE",
        name = "New Game 1"
    });
}
```

## Event Callbacks

```
/// <summary>
/// Meteor returned an error, probably because you have passed it invalid data.
/// </summary>
/// <param name="sender"></param>
/// <param name="e"></param>
static void OnMeteorError(object sender, MeteorError e)
{
    Console.Error.WriteLine("Error: " + e.Reason.ToString());
    Console.Error.WriteLine("Original Message: " + e.OriginalMessage.ToString());
}

/// <summary>
/// The Meteor Client object has thrown an error, most likely your meteor server is unreachable,
/// or you have been disconnected.
/// </summary>
/// <param name="sender"></param>
/// <param name="e"></param>
static void OnClientError(object sender, ClientError e)
{
    Console.WriteLine("Fatal Error: {0}", e.Exception.Message);
    Console.WriteLine("Stack Trace: {0}", e.Exception.StackTrace);
    DdpClient client = (DdpClient)sender;

    ///Disconnected, Handle reconnects here.
    if (client.WebSocketState != WebSocketState.Open)
    {
        Console.WriteLine("Disconnected from Meteor.");
        if (reconnect_if_disconnected)
        { 
            Console.WriteLine("You have enabled reconnect_if_disconencted");
            if (client.WebSocketState != WebSocketState.Open)
            {
                client.Connect();
                if (client.WebSocketState == WebSocketState.Open)
                    MakeSubscriptions();
            }
        }
    }
}

static void OnAdded(object sender, Message msg)
{
    Console.WriteLine("Collection: "+msg.Id + ", row ADDED: "+msg.AffectedRow.ToString());
}

static void onChanged(object sender, Message msg)
{
    Console.WriteLine("Collection: " + msg.Id + ", row CHANGED: " + msg.AffectedRow.ToString());
}

static void onRemoved(object sender, Message msg)
{
    Console.WriteLine("Collection: "+msg.Id + ", row REMOVED: "+ msg.AffectedRow.ToString());
}

static void onSubscribed(object sender, string subscription_id)
{
    Console.WriteLine("Subscription: "+ subscription_id+", READY");
}

static void onUnSubscribed(object sender, Message msg)
{
    Console.WriteLine("Subscription: "+msg.Id + " has been removed.");
}

static void OnMessageResult(object sender, MethodResult e)
{
    Console.WriteLine("Method call result:{0}:  {1}", e.CallId, e.Result);
    if (e.Error != null)
        Console.WriteLine("Error: {0}", e.Error);
}

static void OnConnected(object sender, String msg)
{
    Console.WriteLine(msg);
}
```